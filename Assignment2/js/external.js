 //Validation of Firstname input field
function firstName()
{
var firstname=document.getElementById("fname").value;//catching value contain in input field and storing it into variable named firstname
if(firstname=="")//validation of empty field for firstname and it's value is empty then enter in if loop
{
document.getElementById("fname1").innerHTML="First name is compulsory";//display alert msg for user if user donot enter any name
document.getElementById("fname").style.borderColor = "red";
return false;
}
else//it's value is not empty then enter in else loop
{
document.getElementById("fname1").innerHTML="";// empty alert msg given for user if user enter valid name
document.getElementById("fname").style.borderColor = "";//regain original border color for user if user enter valid name
var regex1 = /^[a-zA-Z]*$/;//Regular expression for only permitting name containg alphabets and restrict number, special characters,alphanumeric characters
var isValid = regex1.test(document.getElementById("fname").value);//checking  value enter by user into firstname field is meeting regular expression or not and storing that output in isValid variable
if (!isValid) //if isValid value is false then it's not is true and it will enter in if loop
{
document.getElementById("fname1").innerHTML="Please enter alphabets only";//display alert msg for user if user donot enter any name
document.getElementById("fname").style.borderColor = "red";
return false;
}
else//if isValid value is true then it's not is false and it will enter in else loop
{
document.getElementById("fname1").innerHTML="";// empty alert msg given for user if user enter valid name
if(firstname.length<2 || firstname.length>10)//checking  value enter by user into firstname field is of length between 2 to 10 and if it's value is true it will enter if loop
{
document.getElementById("fname1").innerHTML="Name should be of length between 2 to 10";//display alert msg for user if user donot enter any name
document.getElementById("fname").style.borderColor = "red";//give border color as red for user if user donot enter any name
return false;
}
else// if it's value is false it will enter else loop
{
document.getElementById("fname1").innerHTML="";// empty alert msg given for user if user enter valid name
document.getElementById("fname").style.borderColor = "";//regain original border color for user if user enter valid name
}  
}
}
}
 
//Validation of Lastname input field
function lastName()
{
var lastname=document.getElementById("lname").value;
if(lastname=="")
{
document.getElementById("lname1").innerHTML="Last name is compulsory";
document.getElementById("lname").style.borderColor = "red";
return false;
}
else
{
document.getElementById("lname1").innerHTML="";
document.getElementById("lname").style.borderColor = "";
var regex2 = /^[a-zA-Z]*$/;
var isValid = regex2.test(document.getElementById("lname").value);
if (!isValid) 
{
document.getElementById("lname1").innerHTML="Please enter alphabets only";
document.getElementById("lname").style.borderColor = "red";
return false;
}
else
{
document.getElementById("lname1").innerHTML="";
document.getElementById("lname").style.borderColor = "";
if(lastname.length<3 || lastname.length>10)
{
document.getElementById("lname1").innerHTML="Name should be of length between 2 to 16";
document.getElementById("lname").style.borderColor = "red";
return false;
}
else
{
document.getElementById("lname1").innerHTML="";
document.getElementById("lname").style.borderColor = "";
}  
}
}
}

//Validation of Phone number input field
function phoneNumber()
{
var phoneno=document.getElementById("pno").value;
if(phoneno=="")//empty field
{
document.getElementById("pno1").innerHTML="Phone number is compulsory";
document.getElementById("pno").style.borderColor = "red";
return false;
}
else
{
document.getElementById("pno1").innerHTML="";
document.getElementById("pno").style.borderColor = "";
if(isNaN(phoneno))//value other than number returns true
{
document.getElementById("pno1").innerHTML="Please enter numbers only";
document.getElementById("pno").style.borderColor = "red";
return false;
}
else
{ 
document.getElementById("pno1").innerHTML="";
document.getElementById("pno").style.borderColor = "";
if(phoneno.length>10 || phoneno.length<10)//value of length 10 only
{
document.getElementById("pno1").innerHTML="Phone number should be exactly of length 10";
document.getElementById("pno").style.borderColor = "red";
return false;
}
else
{
document.getElementById("pno1").innerHTML="";
document.getElementById("pno").style.borderColor = "";
}  
}
}
} 

//Validation of Office number
function officeNumber()
{
var officeno=document.getElementById("offno").value;
if(officeno=="")
{
document.getElementById("offno1").innerHTML="Office number is compulsory";
document.getElementById("offno").style.borderColor = "red";
return false;
}
else
{
document.getElementById("offno1").innerHTML="";
document.getElementById("offno").style.borderColor = "";
if(isNaN(officeno))
{
document.getElementById("offno1").innerHTML="Please enter numbers only";
document.getElementById("offno").style.borderColor = "red";
return false;
}
else
{ 
document.getElementById("offno1").innerHTML="";
document.getElementById("offno").style.borderColor = "";
/*if(officeno.length>10 || officeno.length<10)
{
document.getElementById("offno1").innerHTML="Office number should be exactly of length 10";
document.getElementById("offno").style.borderColor = "red";
return false;
}
else
{
document.getElementById("offno1").innerHTML="";
document.getElementById("offno").style.borderColor = "";
}*/ 
}
}
}

//Validation of Email input field
function emailId()
{
var email=document.getElementById("eml").value;
document.getElementById("eml").style.borderColor = "";
if(email=="")
{
document.getElementById("eml1").innerHTML="Email is compulsory";
document.getElementById("eml").style.borderColor = "red";
return false;
}
else
{
document.getElementById("eml1").innerHTML="";
document.getElementById("eml").style.borderColor = "";
var regexxx = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
var isValid = regexxx.test(document.getElementById("eml").value);
if (!isValid) //if invalid email enter if loop
{
document.getElementById("eml1").innerHTML="Please enter valid emai id";
document.getElementById("eml").style.borderColor = "red";
return false;
}
else
{
document.getElementById("eml1").innerHTML="";
document.getElementById("eml").style.borderColor = "";
}
}    
}
 
//Validation of Password input field
function passward()
{
var passward1=document.getElementById("pass").value;
document.getElementById("pass").style.borderColor = "";
if(passward1=="")//empty field
{
document.getElementById("pass1").innerHTML="Password is compulsory";
document.getElementById("pass").style.borderColor = "red";
return false;
}
else
{
document.getElementById("pass1").innerHTML="";
document.getElementById("pass").style.borderColor = "";
var regex =/^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$/;
var isValid = regex.test(document.getElementById("pass").value);
if (!isValid) //if enter only alphabets or only letters or special char then enter if loop
{
document.getElementById("pass1").innerHTML="Please enter only alphanumeric (ie combination of letters and numbers only) characters";
document.getElementById("pass").style.borderColor = "red";
return false;
}
else
{
if(passward1.length<8 || passward1.length>12)// length should be between 8 to 12 char
{
document.getElementById("pass1").innerHTML="Passward should range between 8 and 12";
document.getElementById("pass").style.borderColor = "red";
return false;
}
else
{
document.getElementById("pass1").innerHTML="";
document.getElementById("pass").style.borderColor = "";
} 
}
}
}

//Validation of Confirm Password input field
function confirmPassword()
{
var passward=document.getElementById("pass").value;
var cpassword=document.getElementById("cpass").value;
if(cpassword=="")
{
document.getElementById("cpass1").innerHTML="Confirm Password is compulsory";
document.getElementById("cpass").style.borderColor = "red";
return false;
}
else
{
document.getElementById("cpass1").innerHTML="";
document.getElementById("cpass").style.borderColor = "";
var regexx = /^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$/;
var isValid = regexx.test(document.getElementById("cpass").value);
if (!isValid) 
{
document.getElementById("cpass1").innerHTML="Please enter only alphanumeric characters";
document.getElementById("cpass").style.borderColor = "red";
return false;
}
else
{
if(cpassword.length<8 || cpassword.length>12)
{
document.getElementById("cpass1").innerHTML="Confirm Passward should range between 8 and 12";
document.getElementById("cpass").style.borderColor = "red";
return false;
}
else
{
document.getElementById("cpass1").innerHTML="";
document.getElementById("cpass").style.borderColor = "";
var cmp= passward.localeCompare(cpassword);
if(cmp!=0)
{
document.getElementById("cpass1").innerHTML="Confirm Password does not match with Password";
document.getElementById("cpass").style.borderColor = "red";
return false;
}
else
{
document.getElementById("cpass1").innerHTML="";
document.getElementById("cpass").style.borderColor = "";
}
} 
}
}   
}

//Validation of radio buttons
function radioButtonn()
{   
if(document.getElementById('residence1').checked) //if male is checked
{
document.getElementById("radio1").innerHTML = "";
}
else if(document.getElementById('residence2').checked) //if female is checked
{
document.getElementById("radio1").innerHTML = "";
}
else// if none is checked
{
document.getElementById("radio1").innerHTML="Please choose your gender";
return false;
}
}

//Validation of checkboxes
function checkBoxn()
{   //if at lest one checkbox is checked
if (document.getElementById("checkbox_sample18").checked || document.getElementById("checkbox_sample19").checked ||document.getElementById("checkbox_sample20").checked) 
{
document.getElementById("checkbox1").innerHTML = "";
} 
else// if none of the checkbox is checked
{
document.getElementById("checkbox1").innerHTML = "Please choose at least one area of interest";
document.getElementById("checkbox1").style.color = "red";
}
}

//Validation of Textarea
function textArea()
{
var tar=document.getElementById('ta').value;
if(tar=="")// if textarea is empty
{
document.getElementById("txtarea1").innerHTML="Please enter some data into about you";
document.getElementById("ta").style.borderColor = "red";
return false;
}
else// if textarea contain data
{
document.getElementById("txtarea1").innerHTML="";
document.getElementById("ta").style.borderColor = "";       
}
}

//Validation of Birth Date
function birthDate() 
{
//checking if user select month or not
var dmy1 = document.getElementById("sm");// select tag
var selectedValue1 = dmy1.options[dmy1.selectedIndex].value;// store option tag's value attribute's value
var dmy2 = document.getElementById("sd");
var selectedValue2 = dmy2.options[dmy2.selectedIndex].value;
var dmy3 = document.getElementById("sy");
var selectedValue3 = dmy3.options[dmy3.selectedIndex].value;
if(selectedValue1 == "selectMonth"&&selectedValue2 == "selectDay"&&selectedValue3 == "selectYear")
{
document.getElementById("select1").innerHTML = "Please select complete birthdate";
return false;
}
else
{
document.getElementById("select1").innerHTML = "";
}
if (selectedValue1 == "selectMonth") 
{
document.getElementById("select1").innerHTML = "Please select birth month";
return false;
}
else 
{
document.getElementById("select1").innerHTML = "";
}
//checking if user select day or not
if (selectedValue2 == "selectDay")
{
document.getElementById("select1").innerHTML = "Please select birth day";
return false;
}
else
{
document.getElementById("select1").innerHTML = "";
}
//checking if user select year or not
if (selectedValue3 == "selectYear")
{
document.getElementById("select1").innerHTML = "Please select birth year";
return false;
}
else 
{
document.getElementById("select1").innerHTML = "";
//Validation of BirthDate
var cMonth, cYear, cDay;
var today = new Date();
cDay = today.getDate();
cMonth = today.getMonth();
cYear = today.getFullYear();
if ((cYear == selectedValue3 && cMonth == selectedValue1 && cDay == selectedValue2)
|| (cYear == selectedValue3 && cMonth == selectedValue1 && cDay < selectedValue2)
||(cYear == selectedValue3 && cMonth < selectedValue1&& cDay == selectedValue2)
||(cYear == selectedValue3 && cMonth < selectedValue1&& cDay != selectedValue2))
{
document.getElementById("select1").innerHTML = "Please select valid BirthDate";
return false;
}
else
{   
//Validation of age     
if ((selectedValue1 == cMonth && selectedValue2 > cDay) || (selectedValue1 == cMonth && selectedValue2 < cDay) || (selectedValue1 == cMonth && selectedValue2 == cDay))   
{
var y1 = cYear - selectedValue3;
var m1 = selectedValue1 - cMonth;
var d1 = cDay - selectedValue2;
document.getElementById("sag1").innerHTML = y1 + "&nbsp;year&nbsp;&nbsp;" + m1 + "&nbsp;months&nbsp;&nbsp;" + d1 + "&nbsp;days";
var age1 = y1+(m1/12)+(d1/365);
document.getElementById("ag").value = age1.toFixed(2);
return false;   
}
if ((selectedValue1 < cMonth && selectedValue2 > cDay) || (selectedValue1 < cMonth && selectedValue2 < cDay) || (selectedValue1 < cMonth && selectedValue2 == cDay)) {
var y1 = cYear - selectedValue3;
var m1 = cMonth - selectedValue1;
var d1 = cDay - selectedValue2;
var age1 = y1+(m1/12)+(d1/365);       
document.getElementById("sag1").innerHTML = y1 + "&nbsp;year&nbsp;&nbsp;" + m1 + "&nbsp;months&nbsp;&nbsp;" + d1 + "&nbsp;days";
document.getElementById("ag").value = age1.toFixed(2);
return false; 
}
if ((selectedValue1 > cMonth && selectedValue2 > cDay) || (selectedValue1 > cMonth && selectedValue2 < cDay) || (selectedValue1 > cMonth && selectedValue2 == cDay)) {
var y1 = cYear - selectedValue3;
var m1 = cMonth - selectedValue1;
var d1 = cDay - selectedValue2;
var age1 = y1+(m1/12)+(d1/365);
document.getElementById("sag1").innerHTML = y1 + "&nbsp;year&nbsp;&nbsp;" + m1 + "&nbsp;months&nbsp;&nbsp;" + d1 + "&nbsp;days";
document.getElementById("ag").value = age1.toFixed(2);
return false;   
}
}
}
}

//Validation of entire form fields from one function
function validateFunction()
{
//calling of all above functions
firstName(); 
lastName();
phoneNumber();
officeNumber();
emailId();
confirmPassword();
radioButtonn();
checkBoxn();
textArea();
birthDate();
passward();
}


