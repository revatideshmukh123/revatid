$(document).ready(function()
{   
$("#fname1").hide();
$("#lname1").hide();
$("#pno1").hide();
$("#offno1").hide();
$("#eml1").hide();
$("#pass1").hide();
$("#cpass1").hide(); 
    $("#checkbox1").click(function() {
    check_box();
    });
    $("#dob").click(function() {
    my_age();
    });
    $("#fname").focusout(function()
    {
    firstName();
    });
    $('#lname').focusout(function()
    {
    lastName();
    });
    $('#pno').focusout(function()
    {
    phoneNumber();
    });
    $('#offno').focusout(function()
    {
    officeNumber();
    });
    $('#eml').focusout(function()
    {
    emailId();
    });
    $('#pass').focusout(function()
    {
    passward();
    });
    $('#cpass').focusout(function()
    {
    confirmPassword();
    });
    $('#ta').focusout(function()
    {
    textArea();
    });
    //validation of submit button
    $("#proceed_button").click(function(){
        check_box();
        firstName();
        lastName();
        phoneNumber();
        officeNumber();
        emailId();
        passward();
        confirmPassword();
        textArea();
        my_age();
        });
//validation of checkboxes
        function check_box() {
        var a = $("#checkbox_sample18").prop("checked");
        var b = $("#checkbox_sample19").prop("checked");
        var c = $("#checkbox_sample20").prop("checked");
        if (a == false && b == false && c == false) {
        $("#check")
        .html("**Please select atleast one.")
        .css("color", "red")
        .show();
        } else {
        $("#check").hide();
        }
        }
 //validation of birthdate and calculation of age       
        function my_age(){
            var d = new Date();
            var cyear = d.getFullYear();
            var cmonth = d.getMonth();
            var cday = d.getDate(); 
            var age1;
            var byear=$('#year1').val();
            var bmonth=$('#month1').val();
            var bday=$('#day1').val();
            if(bmonth == 'selectMonth' || bday == 'selectDay' || byear == 'selectYear'){
            $('#DOB1').text('Please select complete birth date');
            $('#DOB1').show();
            $('#DOB1').css('color','red');
            $('#ag').val("");
            $('#ag').css('border','1px solid red');
            $('#sag1').text("Please select complete birth date"); 
            $('#sag1').css('color','red');
            }
            else
            {
            if((cyear == byear && cmonth ==  bmonth && cday == bday)
            || (cyear == byear  && cmonth ==  bmonth&& cday < bday)
            ||(cyear ==  byear && cmonth <  bmonth&& cday == bday)
            ||(cyear ==   byear && cmonth <  bmonth&& cday != bday)||(cyear<byear))
            {
                $('#DOB1').text('Please select valid BirthDate');
                $('#DOB1').show();
                $('#DOB1').css('color','red');
                $('#ag').css('border','1px solid red');
                $('#ag').val("");
              $('#sag1').text("please select valid birthdate");
              $('#sag1').css('color','red');
            }
            else 
            { 
                if((cyear==byear) || (cyear>byear))
                {
               
                if ((bmonth == cmonth && bday > cday) || (bmonth == cmonth && bday < cday) || (bmonth == cmonth && bday == cday))   
{
var y1 = cyear - byear;
var m1 = bmonth - cmonth;
var d1 = cday - bday;
document.getElementById("sag1").innerHTML = y1 + "&nbsp;year&nbsp;&nbsp;" + m1 + "&nbsp;months&nbsp;&nbsp;" + d1 + "&nbsp;days";
var age1 = y1+(m1/12)+(d1/365);
$('#ag').val(age1.toFixed(2));
$('#DOB1').hide();   
$('#ag').css('border','');
$('#sag1').css('color','');
}
if ((bmonth < cmonth && bday > cday) || (bmonth < cmonth && bday< cday) || (bmonth < cmonth && bday == cday)) {
var y1 = cyear - byear;
var m1 = cmonth - bmonth;
var d1 = cday - bday;
var age1 = y1+(m1/12)+(d1/365);       
document.getElementById("sag1").innerHTML = y1 + "&nbsp;year&nbsp;&nbsp;" + m1 + "&nbsp;months&nbsp;&nbsp;" + d1 + "&nbsp;days";
$('#ag').val(age1.toFixed(2));
$('#DOB1').hide(); 
$('#ag').css('border','');
$('#sag1').css('color','');
}
if ((bmonth > cmonth && bday > cday) || (bmonth > cmonth && bday < cday) || (bmonth > cmonth && bday == cday)) {
var y1 = cyear - byear;
var m1 = cmonth - bmonth;
var d1 = cday - bday;
var age1 = y1+(m1/12)+(d1/365);
document.getElementById("sag1").innerHTML = y1 + "&nbsp;year&nbsp;&nbsp;" + m1 + "&nbsp;months&nbsp;&nbsp;" + d1 + "&nbsp;days";
$('#ag').val(age1.toFixed(2));
$('#DOB1').hide();  
$('#ag').css('border','');
$('#sag1').css('color','');
} 
}  
}
}
}
    
   
    //validation of radio buttons (ie gender)
   $("#proceed_button").click(function() {
    var result = $('input[type="radio"]:checked');
    if (result.length >0 ) {
    $("#radio1").html("");
    } else{
    $("#radio1").html("**Please select at least one interest**");
    $('#radio1').css("color","red");
    }
    });
    $("#residence1").click(function() {
        var result = $('input[type="radio"]:checked');
        if (result.length >0 ) {
        $("#radio1").html("");
        } else{
        $("#radio1").html("**Please select at least one interest**");
        $('#radio1').css("color","red");
        }
        });
        $("#residence2").click(function() {
            var result = $('input[type="radio"]:checked');
            if (result.length >0 ) {
            $("#radio1").html("");
            } else{
            $("#radio1").html("**Please select at least one interest**");
            $('#radio1').css("color","red");
            }
            });

 //validation of firstname  
$("#fname").on("input",function()       
{firstName();});
function firstName(){
    var firstname=$('#fname').val();
    if(firstname==""){
        $('#fname1').text("***This field is mandatory***");
        $('#fname1').show();
        $('#fname1').css("color","red");  
        $('#fname').css("border","1px solid red");
    }
    else{
        if(!firstname.match( /^[a-zA-Z]*$/)){
        $('#fname1').text("***Characters must be alphabets only***");
        $('#fname1').show();
        $('#fname1').css("color","red");   
        $('#fname').css("border","1px solid red");
        }
        else if(firstname.length<2 || firstname.length>10){
        $('#fname1').text("***Characters should between 2 to 10***");
        $('#fname1').show();
        $('#fname1').css("color","red");   $('#fname').css("border","1px solid red");
        }
        else{
        $("#fname1").hide();
        $('#fname').css("border",""); 
        }
    }
}
//validation of lastname
$("#lname").on("input",function() 
{lastName();});
function lastName(){
    var lastname=$('#lname').val();
    if(lastname==""){
        $('#lname1').text("***This field is mandatory***");
        $('#lname1').show();
        $('#lname1').css("color","red");$('#lname').css("border","1px solid red");
    }
    else{
        if(!lastname.match( /^[a-zA-Z]*$/)){
            $('#lname1').text("***Characters must be alphabets only***");
            $('#lname1').show();
            $('#lname1').css("color","red"); $('#lname').css("border","1px solid red");  
        }
        else if(lastname.length<3 || lastname.length>10){
            $('#lname1').text("***Characters should between 3 to 10***");
            $('#lname1').show();
            $('#lname1').css("color","red"); $('#lname').css("border","1px solid red");  
        }
        else{
            $("#lname1").hide(); $('#lname').css("border","");
        }
    }
}
//validation of phone number
$("#pno").on("input",function() {
    phoneNumber();});
    function phoneNumber()
    {
    var phoneno=$('#pno').val();
    if(phoneno==""){
        $('#pno1').text("***This field is mandatory***");
        $('#pno1').show();
        $('#pno1').css("color","red");$('#pno').css("border","1px solid red");
    }
    else{
        if(isNaN(phoneno)){
            $('#pno1').text("***Phone no must be digits only***");
            $('#pno1').show();
            $('#pno1').css("color","red");$('#pno').css("border","1px solid red");   
        }
        else if(phoneno.length>10 || phoneno.length<10){
            $('#pno1').text("***Phone no should of length 10***");
            $('#pno1').show();
            $('#pno1').css("color","red"); $('#pno').css("border","1px solid red");  
        }
        else{
            $("#pno1").hide(); $('#pno').css("border","");
        }
    }
}
//validation of office number
$("#offno").on("input",function() 
{officeNumber();});
function officeNumber()
{
    var officeno=$('#offno').val();
    if(officeno==""){
        $('#offno1').text("***This field is mandatory***");
        $('#offno1').show();
        $('#offno1').css("color","red");$('#offno').css("border","1px solid red");
    }
    else{
        if(isNaN(officeno)){
            $('#offno1').text("***Office no must be digits only***");
            $('#offno1').show();
            $('#offno1').css("color","red"); $('#offno').css("border","1px solid red");  
        }
        else{
            $("#offno1").hide(); $('#offno').css("border","");
        }
    }
}
//validation of email id
$("#eml").on("input",function() 
{emailId();});
function emailId()
{
    var emailid=$('#eml').val();
    if(emailid==""){
        $('#eml1').text("***This field is mandatory***");
        $('#eml1').show();
        $('#eml1').css("color","red");$('#eml').css("border","1px solid red");
    }
    else{
        var regexxx = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        var isValid = regexxx.test(emailid);
        if (!isValid) 
        {
            $('#eml1').text("***Please enter valid email id***");
            $('#eml1').show();
            $('#eml1').css("color","red");$('#eml').css("border","1px solid red");   
        }
        else{
            $("#eml1").hide(); $('#eml').css("border","");
        }
    }  
}
//validation of password
$("#pass").on("input",function() 
{passward();});
function passward()
{
    var passw=$('#pass').val();
    if(passw==""){
        $('#pass1').text("***This field is mandatory***");
        $('#pass1').show();
        $('#pass1').css("color","red");$('#pass').css("border","1px solid red");
    }
    else{
        var regex =/^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$/;
        var isValid = regex.test(passw);
        if(!isValid){
            $('#pass1').text("***Please enter combination of alphabets and digits only***");
            $('#pass1').show();
            $('#pass1').css("color","red");$('#pass').css("border","1px solid red");   
        }
        else if(passw.length<8 || passw.length>12){
            $('#pass1').text("***Password should be of length between 8 to 12***");
            $('#pass1').show();
            $('#pass1').css("color","red");$('#pass').css("border","1px solid red");   
        }
        else{
            $("#pass1").hide(); $('#pass').css("border","");
        }
    } 
}
//validation of confirm password
$("#cpass").on("input",function() 
{confirmPassword();});
function confirmPassword()
{
    var passw=$('#pass').val();
    var cpassw=$('#cpass').val();
    if(cpassw==""){
        $('#cpass1').text("***This field is mandatory***");
        $('#cpass1').show();
        $('#cpass1').css("color","red");$('#cpass').css("border","1px solid red");
    }
    else{
    var cmp= passw.localeCompare(cpassw);
if(cmp!=0)
{
    $('#cpass1').text("***Password and confirm paswword doesn't matched***");
    $('#cpass1').show();
    $('#cpass1').css("color","red");$('#cpass').css("border","1px solid red");
}
else{
    $("#cpass1").hide(); $('#cpass').css("border","");
}
}
}
//validation of textarea
$("#ta").on("input",function() 
{textArea();});
function textArea()
{
    var txtarea=$('#ta').val();  
    if(txtarea=="")
    {
        $('#txtarea1').text("***Please enter something inside about us***");
        $('#txtarea1').show();
        $('#txtarea1').css("color","red"); $('#ta').css("border","1px solid red");
    }
    else{
        $('#txtarea1').hide();$('#ta').css("border","1px solid red");
    }
}
});

