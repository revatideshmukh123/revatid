$(document).ready(function()
{   

$("#uname1").hide();
$("#bday1").hide();
$("#eml1").hide();
$("#pass1").hide();
$("#cpass1").hide();

$('#uname').focusout(function()
{
userName();
});
$('#bday').focusout(function()
{
birthDay();
});
$('#eml').focusout(function()
{
emailId();
});
$('#pass').focusout(function()
{
passward();
});
$('#cpass').focusout(function()
{
confirmPassword();
});
//validation of submit button
$("#p_button").click(function(){ 
   
    userName();
    birthDay();
        emailId();
        passward();
        confirmPassword();
        
    });
    
//validation of username
$("#uname").on("input",function() 
{userName();});
function userName(){
    var username=$('#uname').val();
    if(username==""){
        $('#uname1').text("***This field is mandatory***");
        $('#uname1').show();
        $('#uname1').css("color","red");$('#uname').css("border","1px solid red");
        
    }
    else{
       if(username.length<3 || username.length>16){
            $('#uname1').text("***Characters should between 3 to 16***");
            $('#uname1').show();
            $('#uname1').css("color","red"); $('#uname').css("border","1px solid red");  
            //$('#p_button').attr("href","#");
        }
        else{
            $("#uname1").hide(); $('#uname').css("border","");
           // $('#p_button').attr("href","file:///Users/webwerks/Desktop/revatid/Assignment4/Login_form1.html");
        }
    }
}
//validation of birthday
$("#bday").on("input",function() 
{birthDay();});
function birthDay(){
    var birthday=$('#bday').val();
    if(birthday==""){
        $('#bday1').text("***This field is mandatory***");
        $('#bday1').show();
        $('#bday1').css("color","red");$('#bday').css("border","1px solid red");
      //  $('#p_button').attr("href","#");
    }
    else{
       
            $("#bday1").hide(); $('#bday').css("border","");
            //$('#p_button').attr("href","file:///Users/webwerks/Desktop/revatid/Assignment4/Login_form1.html");
        
    }
}
//validation of email id
$("#eml").on("input",function() 
{emailId();});
function emailId()
{
    var emailid=$('#eml').val();
    if(emailid==""){
        $('#eml1').text("***This field is mandatory***");
        $('#eml1').show();
        $('#eml1').css("color","red");$('#eml').css("border","1px solid red");
        //$('#p_button').attr("href","#");
    }
    else{
        var regexxx = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        var isValid = regexxx.test(emailid);
        if (!isValid) 
        {
            $('#eml1').text("***Please enter valid email id***");
            $('#eml1').show();
            $('#eml1').css("color","red");$('#eml').css("border","1px solid red"); 
            //$('#p_button').attr("href","#");  
        }
        else{
            $("#eml1").hide(); $('#eml').css("border","");  
            //$('#p_button').attr("href","file:///Users/webwerks/Desktop/revatid/Assignment4/Login_form1.html");
        }
    }  
}
//validation of password
$("#pass").on("input",function() 
{passward();});
function passward()
{
    var passw=$('#pass').val();
    if(passw==""){
        $('#pass1').text("***This field is mandatory***");
        $('#pass1').show();
        $('#pass1').css("color","red");$('#pass').css("border","1px solid red");
        //$('#p_button').attr("href","#");
    }
    else{
        var regex =/^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$/;
        var isValid = regex.test(passw);
        if(!isValid){
            $('#pass1').text("***Please enter combination of alphabets and digits only***");
            $('#pass1').show();
            $('#pass1').css("color","red");$('#pass').css("border","1px solid red"); 
            //$('#p_button').attr("href","#");  
        }
        else if(passw.length<8 || passw.length>12){
            $('#pass1').text("***Password should be of length between 8 to 12***");
            $('#pass1').show();
            $('#pass1').css("color","red");$('#pass').css("border","1px solid red");
            // $('#p_button').attr("href","#");  
        }
        else{
            $("#pass1").hide(); $('#pass').css("border",""); 
            // $('#p_button').attr("href","file:///Users/webwerks/Desktop/revatid/Assignment4/Login_form1.html");
        }
    } 
}
//validation of confirm password
$("#cpass").on("input",function() 
{confirmPassword();});
function confirmPassword()
{
    var passw=$('#pass').val();
    var cpassw=$('#cpass').val();
    if(cpassw==""){
        $('#cpass1').text("***This field is mandatory***");
        $('#cpass1').show();
        $('#cpass1').css("color","red");$('#cpass').css("border","1px solid red");
   //$('#p_button').attr("href","#");
    }
    else{
    var cmp= passw.localeCompare(cpassw);
if(cmp!=0)
{
    $('#cpass1').text("***Password and confirm paswword doesn't matched***");
    $('#cpass1').show();
    $('#cpass1').css("color","red");$('#cpass').css("border","1px solid red");
    //$('#p_button').attr("href","#");
}
else{
    $("#cpass1").hide(); $('#cpass').css("border",""); 
    // $('#p_button').attr("href","file:///Users/webwerks/Desktop/revatid/Assignment4/Login_form1.html");
}
}
}

    
});